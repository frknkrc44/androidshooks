// Copyright (C) 2020-2021 Furkan Karcıoğlu <https://gitlab.com/frknkrc44>
//
// This file is part of AndroidSHooks project,
// and licensed under GNU Affero General Public License v3.
// See the GNU Affero General Public License for more details.
//
// All rights reserved. See COPYING, AUTHORS.
//

package org.blinksd.androidshooks;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crossbowffs.remotepreferences.RemotePreferences;

import java.util.concurrent.Executors;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class SMain extends GestureDetector.SimpleOnGestureListener implements IXposedHookLoadPackage {
    private int mNumTiles = Utils.NUM_COLUMNS_DEF;
    private boolean mSquareQS = false;
    private boolean mCustomBrightnessSlider = false;
    private boolean mCustomBrightnessSliderHeightEnabled = false;
    private int mCustomBrightnessSliderHeight = 16;
    private boolean mQSHideTitles = false;
    private boolean mD2TSEnabled = false;
    private boolean mCustomBuildTextEnabled = false;
    private String mCustomBuildText = null;
    private Context mContext;
    private PowerManager mPowerManager;
    private GestureDetector mD2TSDetector;
    private Handler mHandler;
    private View qqsPanelView;

    private void loadSP() {
        try {
            SharedPreferences mSharedPreferences = new RemotePreferences(mContext, PrefProvider.PREF_AUTHORITY, Utils.SHARED_PREF_NAME, true);
            mNumTiles = mSharedPreferences.getInt(Utils.NUM_COLUMNS_KEY, Utils.NUM_COLUMNS_DEF);
            mSquareQS = mSharedPreferences.getBoolean(Utils.SQUARE_QS_KEY, mSquareQS);
            mCustomBrightnessSlider = mSharedPreferences.getBoolean(Utils.CUSTOM_BRIGHTNESS_SLIDER_KEY, mCustomBrightnessSlider);
            mCustomBrightnessSliderHeightEnabled =  mCustomBrightnessSlider && mSharedPreferences.getBoolean(Utils.CUSTOM_BRIGHTNESS_SLIDER_HEIGHT_ENABLED_KEY, mCustomBrightnessSliderHeightEnabled);
            mCustomBrightnessSliderHeight = mSharedPreferences.getInt(Utils.CUSTOM_BRIGHTNESS_SLIDER_HEIGHT_KEY, mCustomBrightnessSliderHeight);
            mD2TSEnabled = mSharedPreferences.getBoolean(Utils.D2TS_ENABLED_KEY, mD2TSEnabled);
            mCustomBuildTextEnabled = mSharedPreferences.getBoolean(Utils.CUSTOM_BUILD_TEXT_ENABLED_KEY, mCustomBuildTextEnabled);
            mCustomBuildText = mSharedPreferences.getString(Utils.CUSTOM_BUILD_TEXT_KEY, mCustomBuildText);
            mQSHideTitles = mSharedPreferences.getBoolean(Utils.QS_HIDE_TITLES, mQSHideTitles);
        } catch (Throwable t) {
            try {
                Thread.sleep(250);
            } catch (Throwable ignored){}
            loadSP();
        }
        System.gc();
    }

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        XposedBridge.log("PKG loaded for: " + lpparam.packageName);
        hookSelf(lpparam);
        // initLauncherMethods(lpparam);

        XposedHelpers.findAndHookMethod(Resources.class, "getFloat", int.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                final int targetRes1 = Utils.getResId(mContext, lpparam.classLoader, Utils.PKG_AINTERNAL, "dimen", "config_wallpaperMaxScale");
                int resArg = (int) param.args[0];

                if (resArg == targetRes1) {
                    // disable wallpaper zoom
                    param.setResult(1.0f);
                } else {
                    super.afterHookedMethod(param);
                }
            }
        });

        if (Utils.PKG_SYSTEMUI.equals(lpparam.packageName)) {
            final Class<?> panelViewControllerClass =
                    XposedHelpers.findClass(Utils.CLASS_SYSUIAPP, lpparam.classLoader);
            XposedBridge.hookAllConstructors(panelViewControllerClass, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    XposedBridge.log("PhoneStatusBarView - " + param.thisObject.toString());
                    mContext = (Context) param.thisObject;
                    mHandler = new Handler(Looper.getMainLooper());
                    runBackground(() -> loadSP(), () -> initSysUiMethods(lpparam.classLoader));
                }
            });
        }
    }

    private void initSysUiMethods(ClassLoader classLoader) {
        if (mD2TSDetector == null) {
            mD2TSDetector = new GestureDetector(mContext, this);
        }

        /*
        if(mQSHideTitles) {
            try {
                XposedHelpers.findAndHookMethod(Utils.CLASS_QSPANEL, classLoader, "setUsingHorizontalLayout", boolean.class, ViewGroup.class, boolean.class, new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        Object mTileLayout = XposedHelpers.getObjectField(param.thisObject, "mTileLayout");
                        XposedHelpers.callMethod(mTileLayout, "setMaxColumns", 6);
                    }
                });
            } catch (Throwable ignored) {}

            try {
                XposedHelpers.findAndHookMethod(Utils.CLASS_QSTILEVIEWIMPL, classLoader, "createAndAddLabels", new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        try {
                            View titles = (View) XposedHelpers.getObjectField(param.thisObject, "labelContainer");
                            titles.setVisibility(View.GONE);
                        } catch (Throwable t) {
                            XposedBridge.log(t);
                        }
                    }
                });
            } catch (Throwable ignored) {}
        }
         */

        XposedHelpers.findAndHookMethod(Resources.class, "getInteger", int.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                final int targetRes1 = Utils.getResId(mContext, classLoader, Utils.PKG_SYSTEMUI, "integer", Build.VERSION.SDK_INT >= 33 ? "quick_qs_panel_max_tiles" : "quick_qs_panel_max_columns");
                final int targetRes2 = Utils.getResId(mContext, classLoader, Utils.PKG_SYSTEMUI, "integer", "quick_settings_num_columns");
                final int targetRes3 = Utils.getResId(mContext, classLoader, Utils.PKG_SYSTEMUI, "integer", "quick_qs_panel_max_rows");
                int resArg = (int) param.args[0];

                if (resArg == targetRes1) {
                    // set quick settings tile count to mNumTiles * 2
                    param.setResult(mNumTiles * 2);
                } else if (resArg == targetRes2 || resArg == targetRes3) {
                    // set quick settings column count to mNumTiles
                    param.setResult(mNumTiles);
                } else {
                    super.afterHookedMethod(param);
                }
            }
        });

        /*
        if (mSquareQS) {
            Class<?> qsTileImplClass = XposedHelpers.findClass(Utils.CLASS_QSTILEVIEWIMPL, classLoader);
            try {
                XposedHelpers.findAndHookMethod(qsTileImplClass, "createTileBackground", new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) {
                        setSquareQS(param, classLoader, param.getResult());
                    }
                });
            } catch (Throwable t){
                XposedBridge.hookAllConstructors(qsTileImplClass, new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        setSquareQS(param, classLoader, XposedHelpers.getObjectField(param.thisObject, "ripple"));
                    }
                });
            }
        }
         */

        /*
        if (mCustomBrightnessSlider) {
            Class<?> qsToggleSeekBarClass = XposedHelpers.findClass(Utils.CLASS_TOGGLESEEKBAR, classLoader);
            Class<?> qsBrightnessSliderViewClass = XposedHelpers.findClass(Utils.CLASS_QSBSLIDERVIEW, classLoader);

            XposedBridge.hookAllMethods(qsBrightnessSliderViewClass, "onFinishInflate", new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    View parentView = (View) param.thisObject;
                    SeekBar seekBar = (SeekBar) XposedHelpers.getObjectField(parentView, "mSlider");
                    Resources resources = parentView.getResources();
                    if (mCustomBrightnessSliderHeightEnabled) {
                        View parent2 = (View) parentView.getParent();
                        View parent3 = parent2 != null ? (View) parent2.getParent() : null;
                        View parent4 = parent3 != null ? (View) parent3.getParent() : null;
                        int height = getCustomSliderHeight(resources);
                        if (parentView != null && parentView.getLayoutParams() != null) parentView.getLayoutParams().height = height;
                        if (parent2 != null && parent2.getLayoutParams() != null) parent2.getLayoutParams().height = height;
                        if (parent3 != null && parent3.getLayoutParams() != null) parent3.getLayoutParams().height = height;
                        if (parent4 != null && parent4.getLayoutParams() != null) parent4.getLayoutParams().height = height;
                        if (seekBar.getLayoutParams() != null) seekBar.getLayoutParams().height = height;
                    }
                }
            });

            XposedBridge.hookAllConstructors(qsToggleSeekBarClass, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    SeekBar seekBar = (SeekBar) param.thisObject;
                    Resources resources = seekBar.getResources();

                    if (mCustomBrightnessSlider) {
                        seekBar.setProgressDrawable(createCustomSeekBarDrawable(classLoader, seekBar.getContext(), resources));
                    }

                    if (mCustomBrightnessSliderHeightEnabled) {
                        if (seekBar.getLayoutParams() != null) seekBar.getLayoutParams().height = getCustomSliderHeight(resources);
                    }
                }
            });

            XposedBridge.hookAllMethods(qsBrightnessSliderViewClass, "applySliderScale", new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    SeekBar seekBar = (SeekBar) XposedHelpers.getObjectField(param.thisObject, "mSlider");
                    Resources resources = seekBar.getResources();
                    seekBar.setProgressDrawable(createCustomSeekBarDrawable(classLoader, seekBar.getContext(), resources));
                }
            });
        }
        */

        /*
        if (mD2TSEnabled) {
            Class<?> phoneStatusBarViewClass = XposedHelpers.findClass(Utils.CLASS_PHONESTATUSBARVIEW, classLoader);
            XposedBridge.hookAllMethods(phoneStatusBarViewClass, "onTouchEvent", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    mD2TSDetector.onTouchEvent((MotionEvent) param.args[0]);
                }
            });

            Class<?> panelViewCtrlTHandlerClass = XposedHelpers.findClass(Utils.CLASS_PANELVIEWCTRLTHANDLER, classLoader);
            XposedBridge.hookAllMethods(panelViewCtrlTHandlerClass, "onTouch", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    mD2TSDetector.onTouchEvent((MotionEvent) param.args[1]);
                }
            });
        }
         */

        Class<?> qsTileLayoutClazz = XposedHelpers.findClass(Utils.CLASS_QSPAGEDTILELAYOUT, classLoader);
        XposedBridge.hookAllMethods(qsTileLayoutClazz, "setMaxColumns", new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                param.args[0] = mNumTiles;
            }
        });

        if(mCustomBuildTextEnabled && !TextUtils.isEmpty(mCustomBuildText)) {
            Class<?> qsFooterViewClass = XposedHelpers.findClass("com.android.systemui.qs.QSFooterView", classLoader);
            XposedHelpers.findAndHookMethod(qsFooterViewClass, "setBuildText", new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    TextView mBuildText = (TextView) XposedHelpers.getObjectField(param.thisObject, "mBuildText");
                    mBuildText.setEnabled(true);
                    mBuildText.setSelected(true);
                    mBuildText.setText(mCustomBuildText);
                }
            });
        }

        /*
        IntentFilter intentFilter = new IntentFilter(Utils.INTENT_GOTOSLEEP);
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                onDoubleTap(null);
            }
        };
        mContext.registerReceiver(receiver, intentFilter);
         */
    }

    /*
    private void initLauncherMethods(XC_LoadPackage.LoadPackageParam lpparam) {
        XposedBridge.log("initLauncher");
        Class<?> launcher3WSTouchListener = XposedHelpers.findClassIfExists(Utils.CLASS_WSTOUCHLISTENER, lpparam.classLoader);
        if (launcher3WSTouchListener != null) {
            XposedBridge.log("WSTouchListener found");
            XposedBridge.hookAllConstructors(launcher3WSTouchListener, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    // XposedBridge.log("WSTouchListener afterHookedMethod");
                    mContext = (Activity) XposedHelpers.getObjectField(param.thisObject, "mLauncher");
                    loadSP();

                    if(mD2TSEnabled) {
                        if(mD2TSDetector == null) {
                            mD2TSDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
                                @Override
                                public boolean onDoubleTap(MotionEvent e) {
                                    Intent intent = new Intent(Utils.INTENT_GOTOSLEEP);
                                    intent.setPackage(Utils.PKG_SYSTEMUI);
                                    mContext.sendBroadcast(intent);
                                    return true;
                                }
                            });
                        }

                        XposedHelpers.findAndHookMethod(launcher3WSTouchListener, "onTouch", View.class, MotionEvent.class, new XC_MethodHook() {
                            @Override
                            protected void beforeHookedMethod(MethodHookParam param1) throws Throwable {
                                mD2TSDetector.onTouchEvent((MotionEvent) param1.args[1]);
                                // XposedBridge.log("WSTouchListener beforeHookedMethod");
                            }
                        });
                    }
                }
            });
        }
    }
     */

    private void setSquareQS(XC_MethodHook.MethodHookParam param, ClassLoader classLoader, Object drawable){
        LinearLayout qsTileLayout = (LinearLayout) param.thisObject;

        try {
            RippleDrawable rippleDrawable = (RippleDrawable) drawable;
            int resId1 = Utils.getResId(mContext, classLoader, Utils.PKG_SYSTEMUI, "id", "background");
            Drawable drawable1 = rippleDrawable.findDrawableByLayerId(resId1);
            setSquareQSToTile(drawable1);

            Drawable drawable2 = rippleDrawable.findDrawableByLayerId(android.R.id.mask);
            setSquareQSToTile(drawable2);

            XposedHelpers.setObjectField(qsTileLayout, "colorBackgroundDrawable", drawable1);
            param.setResult(rippleDrawable);
        } catch (Throwable t) {
            XposedBridge.log(t);
        }
    }

    private void setSquareQSToTile(Drawable drawable1) {
        if (drawable1 instanceof GradientDrawable) {
            ((GradientDrawable) drawable1).setCornerRadius(0);
        } else if (drawable1 instanceof LayerDrawable) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable1;
            for (int i = 0; i < layerDrawable.getNumberOfLayers(); i++) {
                Drawable drawable = layerDrawable.getDrawable(i);
                if (drawable instanceof GradientDrawable) {
                    ((GradientDrawable) drawable).setCornerRadius(0);
                }
            }
        }
    }

    private Drawable createCustomSeekBarDrawable(ClassLoader classLoader, Context context, Resources res) throws NoSuchFieldException, IllegalAccessException {
        GradientDrawable gd1 = new GradientDrawable();
        int resId1 = Utils.getResId(mContext, classLoader, Utils.PKG_SYSTEMUI, "attr", "offStateColor");
        TypedArray ta = context.obtainStyledAttributes(new int[] { resId1 });
        resId1 = ta.getColor(0, 0);

        boolean isNight = (res.getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES;
        if (resId1 == 0) {
            resId1 = res.getColor(isNight ? android.R.color.system_neutral1_800 : android.R.color.system_neutral1_0, context.getTheme());
        }
        gd1.setColor(ColorStateList.valueOf(resId1));
        gd1.setCornerRadius(mSquareQS ? 0 : 1000);
        ta.recycle();

        GradientDrawable gd2 = new GradientDrawable();
        int resId2 = 0;

        try {
            if (!context.getClass().getName().equals(Utils.CLASS_QSBDIALOG)) {
                int underSurfaceColor = Utils.getResId(mContext, classLoader, Utils.PKG_SYSTEMUI, "attr", "underSurfaceColor");
                ta = context.obtainStyledAttributes(new int[] { underSurfaceColor });
                underSurfaceColor = ta.getColor(0, 0);
                if (underSurfaceColor != 0) {
                    isNight = Color.luminance(underSurfaceColor) < 0.5;
                }
                ta.recycle();
            }

            if (!isNight) {
                resId2 = Utils.getResId(mContext, classLoader, Utils.PKG_AINTERNAL, "attr", "colorAccentPrimaryVariant");
                ta = context.obtainStyledAttributes(new int[] { resId2 });
                resId2 = ta.getColor(0, 0);
                ta.recycle();
            }
        } catch (Throwable t) {
            // ignored
        }

        if (resId2 == 0) {
            ta = context.obtainStyledAttributes(new int[] { android.R.attr.colorAccent });
            resId2 = ta.getColor(0, 0);
            ta.recycle();
        }

        gd2.setColor(ColorStateList.valueOf(resId2));
        gd2.setCornerRadius(gd1.getCornerRadius());

        ClipDrawable clipDrawable = new ClipDrawable(gd2, Gravity.START, ClipDrawable.HORIZONTAL);
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[] {gd1, clipDrawable});
        layerDrawable.setId(0, android.R.id.background);
        layerDrawable.setId(1, android.R.id.progress);

        if (mCustomBrightnessSliderHeightEnabled) {
            int height = getCustomSliderHeight(res);
            layerDrawable.setLayerHeight(0, height);
            layerDrawable.setLayerHeight(1, height);
        }

        return layerDrawable;
    }

    private void hookSelf(XC_LoadPackage.LoadPackageParam lpparam) {
        if (!BuildConfig.APPLICATION_ID.equals(lpparam.packageName)) return;

        XposedHelpers.findAndHookMethod(
                MainActivity.class.getName(),
                lpparam.classLoader,
                "isXposedEnabled",
                XC_MethodReplacement.returnConstant(true)
        );
    }

    private int getCustomSliderHeight(Resources res) {
        return (int) res.getDisplayMetrics().density * mCustomBrightnessSliderHeight;
    }

    private PowerManager getPowerManager() {
        if (mPowerManager == null) {
            mPowerManager = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        }
        return mPowerManager;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        XposedHelpers.callMethod(getPowerManager(), "goToSleep", SystemClock.uptimeMillis());
        return true;
    }

    private void runBackground(Runnable doBackground, Runnable onFinished) {
        Executors.newSingleThreadExecutor().execute(() -> {
            doBackground.run();
            mHandler.post(onFinished);
        });
    }
}
