// Copyright (C) 2020-2021 Furkan Karcıoğlu <https://gitlab.com/frknkrc44>
//
// This file is part of AndroidSHooks project,
// and licensed under GNU Affero General Public License v3.
// See the GNU Affero General Public License for more details.
//
// All rights reserved. See COPYING, AUTHORS.
//

package org.blinksd.androidshooks;

import android.content.Context;

import de.robv.android.xposed.XposedHelpers;

public class Utils {
    private Utils() {}

    public static final String NUM_COLUMNS_KEY = "qs_num_columns";
    public static final String SQUARE_QS_KEY = "qs_square";
    public static final String CUSTOM_BRIGHTNESS_SLIDER_KEY = "qs_custom_bslider";
    public static final String CUSTOM_BRIGHTNESS_SLIDER_HEIGHT_ENABLED_KEY = "qs_custom_bslider_height_enabled";
    public static final String CUSTOM_BRIGHTNESS_SLIDER_HEIGHT_KEY = "qs_custom_bslider_height";
    public static final String D2TS_ENABLED_KEY = "sysui_d2ts_enabled";
    public static final String CUSTOM_BUILD_TEXT_ENABLED_KEY = "qs_custom_build_text_enabled";
    public static final String CUSTOM_BUILD_TEXT_KEY = "qs_custom_build_text";
    public static final String QS_HIDE_TITLES = "qs_hide_titles";
    // public static final String DISABLE_WALLPAPER_ZOOM_KEY = "disable_wp_zoom";

    public static final String SHARED_PREF_NAME = "smain";
    public static final String PKG_SYSTEMUI = "com.android.systemui";
    public static final String PKG_AINTERNAL = "com.android.internal";
    public static final String PKG_LAUNCHER3 = "com.android.launcher3";
    public static final String CLASS_SYSUIAPP = PKG_SYSTEMUI + ".SystemUIApplication";
    public static final String CLASS_MEDIAHOST = PKG_SYSTEMUI + ".media.controls.ui.view.MediaHost";
    public static final String CLASS_QSPANEL = PKG_SYSTEMUI + ".qs.QSPanel";
    public static final String CLASS_QQSPANEL = PKG_SYSTEMUI + ".qs.QuickQSPanel";
    public static final String CLASS_QSPANELCTRL = PKG_SYSTEMUI + ".qs.QSPanelController";
    public static final String CLASS_QQSPANELCTRL = PKG_SYSTEMUI + ".qs.QuickQSPanelController";
    public static final String CLASS_QSTILEADAPTER = PKG_SYSTEMUI + ".qs.customize.TileAdapter";
    public static final String CLASS_QSPAGEDTILELAYOUT = PKG_SYSTEMUI + ".qs.PagedTileLayout";
    public static final String CLASS_QSBSLIDERVIEW = PKG_SYSTEMUI + ".settings.brightness.BrightnessSliderView";
    public static final String CLASS_PHONESTATUSBARVIEW = PKG_SYSTEMUI + ".statusbar.phone.PhoneStatusBarView";
    public static final String CLASS_TOGGLESEEKBAR = PKG_SYSTEMUI + ".settings.brightness.ToggleSeekBar";
    public static final String CLASS_PANELVIEWCTRL = PKG_SYSTEMUI + ".statusbar.phone.PanelViewController";
    public static final String CLASS_PANELVIEWCTRLTHANDLER = CLASS_PANELVIEWCTRL + ".TouchHandler";
    public static final String CLASS_QSBDIALOG = PKG_SYSTEMUI + ".settings.brightness.BrightnessDialog";
    public static final String CLASS_MEDIADATAMGR = PKG_SYSTEMUI + ".media.controls.domain.pipeline.MediaDataManager";
    public static final String CLASS_WSTOUCHLISTENER = PKG_LAUNCHER3 + ".touch.WorkspaceTouchListener";
    public static final String INTENT_GOTOSLEEP = PKG_LAUNCHER3 + ".GoToSleep";
    public static final int NUM_COLUMNS_DEF = 2;

    // find resources without resource hooks
    public static int getResId(Context context, ClassLoader classLoader, String pkgName, String type, String resName) throws NoSuchFieldException, IllegalAccessException {
        Class<?> resIntegerClass = XposedHelpers.findClassIfExists( pkgName + ".R$" + type, classLoader);
        if(resIntegerClass != null) {
            try {
                return resIntegerClass.getDeclaredField(resName).getInt(null);
            } catch (Throwable ignored){}
        }

        return context.getResources().getIdentifier(resName, type, pkgName);
    }
}
