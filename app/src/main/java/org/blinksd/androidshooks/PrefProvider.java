// Copyright (C) 2020-2021 Furkan Karcıoğlu <https://gitlab.com/frknkrc44>
//
// This file is part of AndroidSHooks project,
// and licensed under GNU Affero General Public License v3.
// See the GNU Affero General Public License for more details.
//
// All rights reserved. See COPYING, AUTHORS.
//

package org.blinksd.androidshooks;

import com.crossbowffs.remotepreferences.RemotePreferenceFile;
import com.crossbowffs.remotepreferences.RemotePreferenceProvider;

public class PrefProvider extends RemotePreferenceProvider {
    public static String PREF_AUTHORITY = "org.blinksd.androidshooks.prefprovider";
    // public static String[] PREF_ALLOWED_PKGS = { "android", "com.android.systemui" };
    public static String[] PREF_ALLOWED_KEYS = {
            Utils.NUM_COLUMNS_KEY,
            Utils.SQUARE_QS_KEY,
            Utils.CUSTOM_BRIGHTNESS_SLIDER_HEIGHT_KEY,
            Utils.CUSTOM_BRIGHTNESS_SLIDER_HEIGHT_ENABLED_KEY,
            Utils.CUSTOM_BRIGHTNESS_SLIDER_KEY,
            Utils.D2TS_ENABLED_KEY,
            Utils.CUSTOM_BUILD_TEXT_ENABLED_KEY,
            Utils.CUSTOM_BUILD_TEXT_KEY,
            Utils.QS_HIDE_TITLES
    };

    public PrefProvider() {
        super(PREF_AUTHORITY, new RemotePreferenceFile[]{
                new RemotePreferenceFile(Utils.SHARED_PREF_NAME, true)
        });
    }

    @Override
    protected boolean checkAccess(String prefFileName, String prefKey, boolean write) {
        return (!write) && isKeyAllowed(prefKey) /*&& isPkgAllowed(getCallingPackage())*/;
    }

    private boolean isKeyAllowed(String keyName) {
        for (String key : PREF_ALLOWED_KEYS) {
            if (key.equals(keyName))
                return true;
        }

        return false;
    }

    /*
    private boolean isPkgAllowed(String pkgName) {
        for (String pkg : PREF_ALLOWED_PKGS) {
            if (pkg.equals(pkgName))
                return true;
        }

        return false;
    }
     */
}
