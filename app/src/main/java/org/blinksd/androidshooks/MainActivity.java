// Copyright (C) 2020-2021 Furkan Karcıoğlu <https://gitlab.com/frknkrc44>
//
// This file is part of AndroidSHooks project,
// and licensed under GNU Affero General Public License v3.
// See the GNU Affero General Public License for more details.
//
// All rights reserved. See COPYING, AUTHORS.
//

package org.blinksd.androidshooks;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.io.OutputStream;

@SuppressLint("NonConstantResourceId")
public class MainActivity extends Activity implements View.OnClickListener, NumberPicker.OnValueChangeListener, Switch.OnCheckedChangeListener, TextWatcher {
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isXposedEnabled()) {
            TextView view = new TextView(this);
            view.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            view.setGravity(Gravity.CENTER);
            view.setText(R.string.xposed_module_not_enabled);
            view.setTextAppearance(android.R.style.TextAppearance_Material_Medium);
            setContentView(view);
            return;
        }

        sharedPreferences = getSPImpl();
        setContentView(R.layout.activity_main);

        NumberPicker numPicker = findViewById(android.R.id.text1);
        numPicker.setMinValue(1);
        numPicker.setMaxValue(6);
        numPicker.setValue(sharedPreferences.getInt(Utils.NUM_COLUMNS_KEY, Utils.NUM_COLUMNS_DEF));
        numPicker.setOnValueChangedListener(this);

        /*
        Switch squareQSSwitch = findViewById(R.id.switch_square_qs);
        squareQSSwitch.setChecked(sharedPreferences.getBoolean(Utils.SQUARE_QS_KEY, false));
        squareQSSwitch.setOnCheckedChangeListener(this);

        SeekBar customBrightnessSeekBar = findViewById(R.id.seekbar_custom_bslider_height);
        customBrightnessSeekBar.setProgress(sharedPreferences.getInt(Utils.CUSTOM_BRIGHTNESS_SLIDER_HEIGHT_KEY, 16));
        customBrightnessSeekBar.setOnSeekBarChangeListener(this);

        Switch customBrightnessHeightSwitch = findViewById(R.id.switch_custom_bslider_height);
        customBrightnessHeightSwitch.setChecked(sharedPreferences.getBoolean(Utils.CUSTOM_BRIGHTNESS_SLIDER_HEIGHT_ENABLED_KEY, false));
        customBrightnessHeightSwitch.setOnCheckedChangeListener(this);

        setSwitchText();

        Switch customBrightnessSwitch = findViewById(R.id.switch_custom_bslider);
        customBrightnessSwitch.setChecked(sharedPreferences.getBoolean(Utils.CUSTOM_BRIGHTNESS_SLIDER_KEY, false));
        customBrightnessSwitch.setOnCheckedChangeListener(this);

        customBrightnessHeightSwitch.setEnabled(customBrightnessSwitch.isChecked());
        customBrightnessSeekBar.setEnabled(customBrightnessHeightSwitch.isChecked());

        Switch d2tsEnabledSwitch = findViewById(R.id.switch_d2ts_enabled);
        d2tsEnabledSwitch.setChecked(sharedPreferences.getBoolean(Utils.D2TS_ENABLED_KEY, false));
        d2tsEnabledSwitch.setOnCheckedChangeListener(this);

        Switch qsHideTitles = findViewById(R.id.switch_hide_qs_titles);
        qsHideTitles.setChecked(sharedPreferences.getBoolean(Utils.QS_HIDE_TITLES, false));
        qsHideTitles.setOnCheckedChangeListener(this);
         */

        Switch customBuildTextEnabled = findViewById(R.id.switch_custom_build_text_enabled);
        customBuildTextEnabled.setChecked(sharedPreferences.getBoolean(Utils.CUSTOM_BUILD_TEXT_ENABLED_KEY, false));
        customBuildTextEnabled.setOnCheckedChangeListener(this);

        EditText customBuildText = findViewById(R.id.edittext_custom_build_text);
        customBuildText.setText(sharedPreferences.getString(Utils.CUSTOM_BUILD_TEXT_KEY, ""));
        customBuildText.setEnabled(customBuildTextEnabled.isChecked());
        customBuildText.addTextChangedListener(this);

        Button applyBtn = findViewById(android.R.id.button1);
        applyBtn.setOnClickListener(this);
    }

    private SharedPreferences getSPImpl() {
        Context prefContext = createDeviceProtectedStorageContext();
        if (prefContext == null) prefContext = this;
        return prefContext.getSharedPreferences(Utils.SHARED_PREF_NAME, MODE_PRIVATE);
    }

    @Override
    public void onClick(View v) {
        try {
            java.lang.Process process = Runtime.getRuntime().exec("su");
            OutputStream os = process.getOutputStream();
            os.write("killall com.android.systemui\n".getBytes());
            os.write("killall com.google.android.apps.nexuslauncher\n".getBytes());
            os.write("killall com.android.launcher3\n".getBytes());
            os.write("exit\n".getBytes());
            os.flush();
            os.close();
            process.waitFor();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public boolean isXposedEnabled() {
        return false;
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        sharedPreferences.edit().putInt(Utils.NUM_COLUMNS_KEY, newVal).apply();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        String key;
        switch (buttonView.getId()) {
            /*
            case R.id.switch_square_qs:
                key = Utils.SQUARE_QS_KEY;
                break;
            case R.id.switch_custom_bslider:
                key = Utils.CUSTOM_BRIGHTNESS_SLIDER_KEY;
                Switch customBrightnessHeightSwitch = findViewById(R.id.switch_custom_bslider_height);
                customBrightnessHeightSwitch.setEnabled(isChecked);
                if (!isChecked) customBrightnessHeightSwitch.setChecked(false);
                break;
            case R.id.switch_custom_bslider_height:
                key = Utils.CUSTOM_BRIGHTNESS_SLIDER_HEIGHT_ENABLED_KEY;
                SeekBar customBrightnessSeekBar = findViewById(R.id.seekbar_custom_bslider_height);
                customBrightnessSeekBar.setEnabled(isChecked);
                setSwitchText();
                break;
            case R.id.switch_d2ts_enabled:
                key = Utils.D2TS_ENABLED_KEY;
                break;
            case R.id.switch_hide_qs_titles:
                key = Utils.QS_HIDE_TITLES;
                break;
             */
            case R.id.switch_custom_build_text_enabled:
                key = Utils.CUSTOM_BUILD_TEXT_ENABLED_KEY;
                EditText customBuildText = findViewById(R.id.edittext_custom_build_text);
                customBuildText.setEnabled(isChecked);
                break;
            default:
                return;
        }

        sharedPreferences.edit().putBoolean(key, isChecked).apply();
    }

    /*
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar.getId() == R.id.seekbar_custom_bslider_height) {
            setSwitchText();
        }
    }

    public void setSwitchText() {
        Switch customBrightnessHeightSwitch = findViewById(R.id.switch_custom_bslider_height);
        String defaultText = getString(R.string.title_custom_bslider_height);
        if (customBrightnessHeightSwitch.isChecked()) {
            SeekBar customBrightnessSeekBar = findViewById(R.id.seekbar_custom_bslider_height);
            defaultText += ": " + customBrightnessSeekBar.getProgress();
        }
        customBrightnessHeightSwitch.setText(defaultText);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (seekBar.getId() == R.id.seekbar_custom_bslider_height) {
            sharedPreferences.edit().putInt(
                    Utils.CUSTOM_BRIGHTNESS_SLIDER_HEIGHT_KEY, seekBar.getProgress()).apply();
        }
    }
     */

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        sharedPreferences.edit().putString(Utils.CUSTOM_BUILD_TEXT_KEY, s.toString()).apply();
    }

    @Override
    public void afterTextChanged(Editable s) {}
}