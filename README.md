# Android S Hooks

Another Xposed module

## Functions

- Adjust tile count
- Disable wallpaper zoom
- Custom brightness slider (with custom height selector)
- Double tap to sleep (status bar, lockscreen, quick settings)

## Thanks
- GravityBox
- Xposed Framework
- All testers
